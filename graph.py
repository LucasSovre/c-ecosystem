import csv
import matplotlib.pyplot as plt

# Initialize lists to store the data
wolf_counts = []
sheep_counts = []
tour_counts = []

# Read data from CSV file
with open('results.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Skip the header

    for row in reader:
        wolf_count = int(row[1])
        sheep_count = int(row[2])
        tour_count = int(row[3])

        wolf_counts.append(wolf_count)
        sheep_counts.append(sheep_count)
        tour_counts.append(tour_count)

# Create scatter plot
plt.scatter(wolf_counts, sheep_counts, c=tour_counts, cmap='viridis')
plt.colorbar(label='Number of Tours')
plt.xlabel('Number of Wolves')
plt.ylabel('Number of Sheep')
plt.title('Number of Tours vs. Number of Wolves and Sheep')
plt.grid(True)

# Display the plot
plt.show()
