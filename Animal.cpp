#include "Animal.h"

void Animal::move(Case *next_case) {
    next_case->enter(this);
    this->m_case->leave();
    this->m_case = next_case;
}

std::string Animal::display() {
    return "A";
}

std::vector<Case *> Animal::observe(Grid *grid) {
    std::vector<Case *> result;
    try {
        result.push_back(grid->get_case(this->where()->get_x()-1,this->where()->get_y()+1));
    }
    catch (...){}
    try {
        result.push_back(grid->get_case(this->where()->get_x(),this->where()->get_y()+1));
    }
    catch (...){}
    try {
        result.push_back(grid->get_case(this->where()->get_x()+1,this->where()->get_y()+1));
    }
    catch (...){}
    try {
        result.push_back(grid->get_case(this->where()->get_x()-1,this->where()->get_y()));
    }
    catch (...){}
    try {
        result.push_back(grid->get_case(this->where()->get_x()+1,this->where()->get_y()));
    }
    catch (...){}
    try {
        result.push_back(grid->get_case(this->where()->get_x()-1,this->where()->get_y()-1));
    }
    catch (...){}
    try {
        result.push_back(grid->get_case(this->where()->get_x(),this->where()->get_y()-1));
    }
    catch (...){}
    try {
        result.push_back(grid->get_case(this->where()->get_x()+1,this->where()->get_y()-1));
    }
    catch (...){}
    return result;
}

void Animal::instinct(Grid *grid) {};

void Animal::die() {
    this->m_case->set_salt(true);
    this->m_case->leave();
    delete this;
}