//
// Created by lucas on 03/04/23.
//

#ifndef PROJET_SHEEP_H
#define PROJET_SHEEP_H
#include "Animal.h"

class Sheep: public Animal {

    public:
        explicit Sheep(Case * m_case): Animal(m_case){
            this->m_max_age=50;
        };
        explicit Sheep(bool sex,Case * m_case): Animal(sex,m_case) {
            this->m_max_age=50;
        };
        void eat() override;
        std::string display() override;
        void instinct(Grid *grid) override;
        void die() override;
        void reproduce(Animal *partner, Case *_case) override;
};


#endif //PROJET_SHEEP_H
