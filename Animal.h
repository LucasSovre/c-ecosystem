//
// Created by lucas on 27/03/23.
//
#pragma once
#ifndef PROJET_ANIMAL_H
#define PROJET_ANIMAL_H
#include <iostream>
#include <random>
#include <cstdlib>
#include <ctime>
#include "Case.h"

class Grid;

class Animal{

protected:
    bool m_sex;
    int m_last_meal;
    int m_old;
    Case * m_case;
    int m_max_age;
    int m_imobilized_day;
public:
    explicit Animal(Case * m_case) {
        std::default_random_engine engine(std::time(nullptr));
        std::uniform_int_distribution<int> distribution(0, 1);
        int r = distribution(engine);
        if(r==0){
            this->m_sex = true;
        }
        else{
            this->m_sex = false;
        }
        this->m_last_meal = 0;
        this->m_old = 0;
        this->m_case = m_case;
        this->m_imobilized_day=0;
    }
    explicit Animal(bool sex,Case * m_case){
        this->m_sex=sex;
        this->m_last_meal = 0;
        this->m_old = 0;
        this->m_case=m_case;
        this->m_imobilized_day=0;
    }
    virtual void eat(){};
    virtual std::string display();
    virtual void instinct(Grid* grid);
    virtual std::vector<Case*> observe(Grid* grid);
    void move(Case *next_case);
    virtual void reproduce(Animal* partner,Case* _case){};
    virtual void die();
    Case * where(){
        return this->m_case;
    }
    virtual void killed(){
        this->m_case->leave();
        delete this;
    };
    int get_last_meal(){
        return this->m_last_meal;
    };
    char get_sex(){
        return this->m_sex;
    }
    void grow_old(){
        this->m_old++;
        if(this->m_old > this->m_max_age){
            this->die();
        }
    }
    bool is_imobilized(){
        return this->m_imobilized_day > 0;
    }
};

#endif //PROJET_ANIMAL_H