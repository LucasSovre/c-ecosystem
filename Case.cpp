//
// Created by Lucas Sovre on 16/04/2023.
//

#include "Case.h"
#include "Animal.h"
#include <string>
#include "common.h"

std::vector<std::string> c_grow;

Case::Case(){
    this->has_grass= true;
    this->has_salt= false;
    this->occupant = nullptr;;
    this->y = -1;
    this->x = -1;
}

Case::Case(int x, int y){
    this->has_grass= true;
    this->has_salt= false;
    this->occupant = nullptr;
    this->y = y;
    this->x = x;
}

void Case::grow(){
    if(this->has_salt){
        this->has_grass = true;
        this->has_salt = false;
        c_grow.push_back(this->coordinates());
    }
};

void Case::eaten() {
    if(this->has_grass)
        this->has_grass= false;
    else{
        throw std::runtime_error("There is no grass to eat.");
    }
}

bool Case::is_empty() {
    return this->occupant == nullptr;
}

Animal * Case::get_occupant() {
    return this->occupant;
}

void Case::leave() {
    this->occupant=nullptr;
}

void Case::enter(Animal *animal) {
    if(this->occupant == nullptr) {
        this->occupant=animal;
    }else{
        throw std::runtime_error("Case is already occuped");
    }
}

void Case::set_salt(bool value) {
    this->has_salt= true;
}

int Case::get_x(){
    return this->x;
};
int Case::get_y(){
    return this->y;
};

std::string Case::display(){
    if(this->is_empty()) {
        if(this->has_grass){
            return " G ";
        }else{
            return "   ";
        }
    }else{
        return " " + this->occupant->display() + " ";
    }
}

bool Case::can_be_eaten(){
    return this->has_grass;
};

std::string Case::coordinates()
{
    std::string result;
    result += 'A' + y; // Convert the y coordinate to the corresponding character ('A' for y = 0, 'B' for y = 1, and so on)
    result += std::to_string(x + 1); // Convert the x coordinate to a string and add 1 (to match the 1-based indexing)
    return result;
}