#ifndef C_ECOSYSTEM_CASE_H
#define C_ECOSYSTEM_CASE_H
#include <iostream>
#include <string>
#include "Grid.h"

class Animal;

class Case {
public:
    Case();
    Case(int x, int y);
    void grow();
    void eaten();
    bool is_empty();
    Animal * get_occupant();
    void leave();
    void enter(Animal * animal);
    void set_salt(bool value);
    int get_x();
    int get_y();
    std::string coordinates();
    bool can_be_eaten();
    std::string display();


    friend std::ostream& operator<<(std::ostream& os, const Case& _case){
        os << "Case at x=" << _case.x << "\ty=" << _case.y;
        return os;
    };

private:
    Animal * occupant;
    bool has_grass;
    bool has_salt;
    int x;
    int y;
};

#endif //C_ECOSYSTEM_CASE_H
