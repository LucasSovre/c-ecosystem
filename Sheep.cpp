//
// Created by lucas on 03/04/23.
//

#include "Sheep.h"
#include <random>
#include "common.h"

std::vector<std::string> s_eat;
std::vector<std::string> s_reproduce;
std::vector<std::string> s_die;

void Sheep::die(){
    s_die.push_back(this->m_case->coordinates());
    Animal::die();
}

void Sheep::eat() {
    this->m_case->eaten();
    this->m_last_meal = 0;
    s_eat.push_back(this->m_case->coordinates());
}

std::string Sheep::display() {
    return "S";
}

void Sheep::reproduce(Animal* partner,Case* _case){
    if(partner->get_sex() != this->get_sex()){
        _case->enter(new Sheep(_case));
        this->m_imobilized_day = 2;
        s_reproduce.push_back(_case->coordinates());
    }
}

void Sheep::instinct(Grid *grid){
    std::vector<Case *> around = this->observe((Grid *) grid);
    std::vector<Case *> free_around;
    Sheep * partner = nullptr;
    for(int i=0;i<around.size();i++){
        if(!around.at(i)->is_empty()){
            if(around.at(i)->get_occupant()->display() == "S" && around.at(i)->get_occupant()->get_sex()!=this->get_sex()){
                partner = dynamic_cast<Sheep *>(around.at(i)->get_occupant());
            }
        }else{
            free_around.push_back(around.at(i));
        }
    }

    //move
    if(!free_around.empty()){
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<int> dis(0, free_around.size()-1);
        int random_ind = dis(gen);
        if(partner!= nullptr){
            this->reproduce(partner,free_around.at(random_ind));
        }
        if(!this->is_imobilized()){
            this->move(free_around.at(random_ind));
        }
    }
    this->m_last_meal++;
    if(this->where()->can_be_eaten()){
        this->eat();
    }
    this->grow_old();
    this->m_imobilized_day--;
    if(this->get_last_meal() > 5){
        this->die();
    }
}