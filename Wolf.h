//
// Created by lucas on 27/03/23.
//

#ifndef PROJET_WOLF_H
#define PROJET_WOLF_H
#include "Animal.h"

class Wolf: public Animal{

public:
    explicit Wolf(Case * m_case): Animal(m_case){
        this->m_max_age=60;
    };
    explicit Wolf(bool sex,Case * m_case): Animal(sex,m_case) {
        this->m_max_age=60;
    };
    std::string display();
    void instinct(Grid *grid);
    void eat(Animal * animal);
    void reproduce(Animal *partner, Case *_case);

    void die() override;
};


#endif //PROJET_WOLF_H
