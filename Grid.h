#ifndef C_ECOSYSTEM_GRID_H
#define C_ECOSYSTEM_GRID_H
#import "Case.h"
#include "string"

class Case;

class Grid {
    private:
        std::vector< std::vector<Case *> > m_grid; //liste de liste de cases
        int day;
    public:
        Grid(int width,int nb_wolf,int nb_sheep);
        int get_height();
        int get_width();
        Case* get_case(int, int);
        int play();
        void display();
        ~Grid();
    };

#endif //C_ECOSYSTEM_GRID_H