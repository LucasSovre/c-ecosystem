//
// Created by leo76 on 27/03/2023.
//

#include "Grid.h"
#include "Wolf.h"
#include "Sheep.h"
#include <random>
#include <iostream>
#include <list>
#include <algorithm> //pour find
#include "common.h"


    std::list<int> listRandom(int nbElem, int min, int max); // permet d'utiliser la fonction avant sa définition

    Grid::Grid(int width,int nb_wolf,int nb_sheep)
    {
        if(width*width < nb_sheep+nb_wolf){
            throw std::runtime_error("Insufficient grid space for the given number of sheep and wolves.");
        }
        this->day = 0;
        this->m_grid=std::vector<std::vector<Case *>>();
        for(int i=0;i<width;i++){
            this->m_grid.push_back(std::vector<Case *>());
            for(int j=0;j<width;j++){
                this->m_grid.at(i).push_back(new Case(i,j));
            }
        }

        std::random_device rd;
        std::mt19937 gen(rd());
        int max = this->get_height()*this->get_width() - 1;
        std::uniform_int_distribution<int> dis(0, max);
        std::vector<Case *> arr;
        for(int i=0;i<width;i++){
            for(int j=0;j<width;j++){
                arr.push_back(this->m_grid.at(i).at(j));
            }
        }

        bool sex = true;
        for (int k = 0; k < nb_wolf; k++) {
            int randomIndex = dis(gen);
            arr.at(randomIndex)->enter(new Wolf(sex,arr.at(randomIndex)));
            sex = !sex;
            arr.erase(arr.begin() + randomIndex);
            max --;
            dis.param(std::uniform_int_distribution<int>::param_type(0, max));
        }

        for (int k = 0; k < nb_sheep; k++) {
            int randomIndex = dis(gen);
            arr.at(randomIndex)->enter(new Sheep(sex,arr.at(randomIndex)));
            sex = !sex;
            arr.erase(arr.begin() + randomIndex);
            max --;
            dis.param(std::uniform_int_distribution<int>::param_type(0, max));
        }
    }


    int Grid::get_height()
    {
        return this->m_grid.at(0).size();
    }

    int Grid::get_width()
    {
        return this->m_grid.size();
    }

    Case* Grid::get_case(int x, int y)
    {
        return this->m_grid.at(x).at(y);
    }

void Grid::display()
{
    std::string result;

    // Add column indices (1, 2, 3, ...)
    for (int j = 0; j < this->get_width(); j++) {
        result += "   ";
        result += std::to_string(j + 1);
    }
    result += "\n";

    // Add top border
    result += " ";
    for (int j = 0; j < this->get_width(); j++) {
        result += "+---";
    }
    result += "+\n";

    for (int i = 0; i < this->get_height(); i++) {
        // Add row indices (A, B, C, ...)
        char row_index = 'A' + i;
        result += row_index;
        result += "|";

        for (int j = 0; j < this->get_width(); j++) {
            result += this->m_grid.at(i).at(j)->display();
            result += "|";
        }
        result += "\n";

        // Add horizontal borders
        result += " ";
        for (int j = 0; j < this->get_width(); j++) {
            result += "+---";
        }
        result += "+\n";
    }
    if(!s_eat.empty()){
        result += "\nSheep have eaten " + getStringVector(s_eat) + " grass cases.";
        s_eat.clear();
    }
    if(!w_eat.empty()){
        result += "\nWolf have eaten sheep on cases " + getStringVector(w_eat) + ".";
        w_eat.clear();
    }
    if(!s_die.empty()){
        result += "\nSheep has died on case " + getStringVector(s_die) + ".";
        s_die.clear();
    }
    if(!w_die.empty()){
        result += "\nWolf has died on case " + getStringVector(w_die) + ".";
        w_die.clear();
    }
    if(!c_grow.empty()){
        result += "\nGrass has grown on cases " + getStringVector(c_grow) + ".";
        c_grow.clear();
    }
    if(!s_reproduce.empty()){
        result += "\nNew sheep has been born on cases " + getStringVector(s_reproduce) + ".";
        s_reproduce.clear();
    }
    if(!w_reproduce.empty()){
        result += "\nNew Wolf has been born on cases " + getStringVector(w_reproduce) + ".";
        w_reproduce.clear();
    }
    result += "\n";
    std::cout << result;
}


int Grid::play()
    {
        this->display();
        std::cout << "\nSITUATION INITIALE\n\n";
        bool ended = false;
        while (!ended) {
            for (int i = 0; i < this->get_width(); i++) {
                for (int j = 0; j < this->get_width(); j++) {
                    Case *currentCase = this->m_grid.at(i).at(j);
                    currentCase->grow(); // si il y a du sel sur la case l'herbe va pousser
                    if (!currentCase->is_empty()) {
                        currentCase->get_occupant()->instinct(this);
                    }
                }
            }
            int w_count = 0;
            int s_count = 0;
            for (int i = 0; i < this->get_width(); i++) {
                for (int j = 0; j < this->get_width(); j++) {
                    Case *currentCase = this->m_grid.at(i).at(j);
                    if (!currentCase->is_empty()) {
                        if (currentCase->get_occupant()->display() == "S") {
                            s_count++;
                        }
                        if (currentCase->get_occupant()->display() == "W") {
                            w_count++;
                        }
                    }
                }
            }
            if (w_count == 0 && s_count == 0) {
                ended = true;
            }
            this->display();
            std::cout << "\nTour : " << this->day << "\t|\twolfs : " << w_count << "\t|\tsheeps : " << s_count
                      << "\n\n";
            this->day++;
        }
        return this->day;
    }

    Grid::~Grid() {
        // Delete all Case objects
        for (std::vector<Case*>& row : m_grid) {
            for (Case* casePtr : row) {
                delete casePtr;
            }
        }
    }

//
// Created by leo76 on 27/03/2023.
//
