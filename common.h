//
// Created by Lucas Sovre on 27/05/2023.
//

#ifndef ANIMALSIMULATION_COMMON_H
#define ANIMALSIMULATION_COMMON_H
#include <string>
#include <sstream>

extern std::vector<std::string> s_eat;
extern std::vector<std::string> w_eat;
extern std::vector<std::string> c_grow;
extern std::vector<std::string> s_reproduce;
extern std::vector<std::string> w_reproduce;
extern std::vector<std::string> s_die;
extern std::vector<std::string> w_die;

inline std::string getStringVector(const std::vector<std::string>& strings)
{
    std::ostringstream oss;
    oss << '[';
    for (size_t i = 0; i < strings.size(); i++) {
        oss << strings[i];
        if (i != strings.size() - 1) {
            oss << ",";
        }
    }
    oss << ']';

    return oss.str();
}

#endif //ANIMALSIMULATION_COMMON_H
