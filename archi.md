```mermaid
    classDiagram
    class Grid {
            - m_grid: std::vector<std::vector<Case*>>
            - day: int
            + Grid(width: int, nb_wolf: int, nb_sheep: int)
            + get_height(): int
            + get_width(): int
            + get_case(row: int, col: int): Case*
            + play(): int
            + display(): void
            + ~Grid()
        }
    class Animal {
        - bool m_sex
        - int m_last_meal
        - int m_old
        - Case *m_case
        - int m_max_age
        - int m_imobilized_day
        + Animal(Case *m_case)
        + Animal(bool sex, Case *m_case)
        + eat() : void
        + display() : string
        + instinct(Grid *grid) : void
        + observe(Grid *grid) : vector<Case*>
        + move(Case *next_case) : void
        + reproduce(Animal* partner, Case* _case) : void
        + die() : void
        + where() : Case*
        + killed() : void
        + get_last_meal() : int
        + get_sex() : char
        + grow_old() : void
        + is_imobilized() : bool
    }
    class Case {
        - Animal *occupant
        - bool has_grass
        - bool has_salt
        - int x
        - int y
        + Case()
        + Case(int x, int y)
        + grow() : void
        + eaten() : void
        + is_empty() : bool
        + get_occupant() : Animal*
        + leave() : void
        + enter(Animal *animal) : void
        + set_salt(bool value) : void
        + get_x() : int
        + get_y() : int
        + coordinates() : string
        + can_be_eaten() : bool
        + display() : string
    }
    class Sheep {
        + Sheep(Case *m_case)
        + Sheep(bool sex, Case *m_case)
        + eat() : void
        + display() : string
        + instinct(Grid *grid) : void
        + die() : void
        + reproduce(Animal *partner, Case *_case) : void
    }
    class Wolf {
        + Wolf(Case *m_case)
        + Wolf(bool sex, Case *m_case)
        + display() : string
        + instinct(Grid *grid) : void
        + eat(Animal *animal) : void
        + reproduce(Animal *partner, Case *_case) : void
    }
    Wolf <|-- Animal
    Sheep <|-- Animal
    Case "n" *-- "1" Grid
    Case "1" *-- "1" Animal
```