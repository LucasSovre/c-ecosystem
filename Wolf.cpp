//
// Created by lucas on 27/03/23.
//

#include "Wolf.h"
#include <iostream>
#include <random>
#include <cstdlib>
#include <ctime>
#include "common.h"

std::vector<std::string> w_eat;
std::vector<std::string> w_reproduce;
std::vector<std::string> w_die;

void Wolf::die(){
    w_die.push_back(this->m_case->coordinates());
    Animal::die();
}

std::string Wolf::display() {
    return "W";
}

void Wolf::reproduce(Animal* partner,Case* _case){
    if(partner->get_sex() != this->get_sex()){
        _case->enter(new Wolf(_case));
        this->m_imobilized_day = 2;
        w_reproduce.push_back(_case->coordinates());
    }
}

void Wolf::eat(Animal * animal) {
    this->m_last_meal=0;
    w_eat.push_back(animal->where()->coordinates());
    animal->killed();
}

void Wolf::instinct(Grid *grid){
    std::vector<Case *> around = this->observe((Grid *) grid);
    std::vector<Case *> free_around;
    for(int i=0;i<around.size();i++){
        if(!around.at(i)->is_empty()){
            if(around.at(i)->get_occupant()->display() == "W"){
                //reproduce
                //return;
            }
        }else{
            // add case to free vector
            free_around.push_back(around.at(i));
        }
    }
    //move
    if(!free_around.empty()){
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<int> dis(0, free_around.size()-1);
        int random_ind = dis(gen);
        this->move(free_around.at(random_ind));
    }
    for(int i=0;i<around.size();i++){
        if(!around.at(i)->is_empty()){
            std::string a = around.at(i)->get_occupant()->display();
            if(around.at(i)->get_occupant()->display() == "S"){
                this->eat(around.at(i)->get_occupant());
                break;
            }
        }
    }
    this->grow_old();
    this->m_imobilized_day--;
    if(this->get_last_meal() > 10){
        this->die();
    }
}