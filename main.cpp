#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "Wolf.h"
#include "Sheep.h"
#include "Case.h"

int main(){
    ofstream outputFile("results.csv"); // Open the output file

    outputFile << "Grid Size, Number of Wolves, Number of Sheep, Tour" << endl; // Write the header

    vector<int> gridSizes = {10}; // Different grid sizes to test
    vector<int> wolfCounts;
    for (int i = 0; i <= 100; ++i) {
        wolfCounts.push_back(i);
    }  // Different numbers of wolves to test
    vector<int> sheepCounts;
    for (int i = 0; i <= 100; ++i) {
        sheepCounts.push_back(i);
    }   // Different numbers of sheep to test


    for (int gridSize : gridSizes) {
        for (int wolfCount : wolfCounts) {
            for (int sheepCount : sheepCounts) {
                try {
                    Grid grid(gridSize, wolfCount, sheepCount); // Create the grid

                    int tour = grid.play(); // Play the game and get the number of tours

                    // Write the results to the CSV file
                    outputFile << gridSize << ", " << wolfCount << ", " << sheepCount << ", " << tour << endl;
                }catch (...){}
            }
        }
    }

    outputFile.close(); // Close the output file

    return 0;
}
